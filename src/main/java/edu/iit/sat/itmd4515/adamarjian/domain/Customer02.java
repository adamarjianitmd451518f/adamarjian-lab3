/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author adama
 */
@Entity
@Table(name="customer")
public class Customer02 {
    @Id @ GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long customerId;   
    private String city;



    public Customer02() {
    }

    public Customer02(Long customerId, String city) {
        this.customerId = customerId;
        this.city = city;
    }
    
    /**
     * Get the value of customerId
     *
     * @return the value of customerId
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Set the value of customerId
     *
     * @param customerId new value of customerId
     */
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Customer02{" + "customerId=" + customerId + ", city=" + city + '}';
    }
}
