/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.service;

import edu.iit.sat.itmd4515.adamarjian.domain.Artist;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author adama
 */
@Stateless
public class ArtistService {
    @PersistenceContext(unitName = "itmd4515PU")
    private EntityManager em;

    public ArtistService() {
    }
    
     public Artist findArtistById(Long id){
        return em.find(Artist.class, id);
    }
     
     public List<Artist> findArtist(){
        return em.createQuery("select e from Artist e", Artist.class).getResultList();
    }
    
}
