/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.service;

import edu.iit.sat.itmd4515.adamarjian.domain.Customer02;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author adama
 */
@Stateless
public class CustomerService02 {
     @PersistenceContext(unitName = "itmd4515PU")
     private EntityManager em;

    public CustomerService02() {
    }

    public Customer02 findCustomersById(Long id){
        return em.find(Customer02.class, id);
    }

    public List<Customer02> findCustomers(){
        return em.createQuery("select e from Customer02 e", Customer02.class).getResultList();
    }
     
     
    
}
