/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author adama
 */
@Entity
@Table(name="artist")
public class Artist {
    

    @Id @ GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ArtistId;
    private String name;

    public Artist() {
    }

    public Artist(Long ArtistId, String name) {
        this.ArtistId = ArtistId;
        this.name = name;
    }

    /**
     * Get the value of ArtistId
     *
     * @return the value of ArtistId
     */
    public Long getArtistId() {
        return ArtistId;
    }

    /**
     * Set the value of ArtistId
     *
     * @param ArtistId new value of ArtistId
     */
    public void setArtistId(Long ArtistId) {
        this.ArtistId = ArtistId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Artist{" + "ArtistId=" + ArtistId + ", name=" + name + '}';
    }

    
}
