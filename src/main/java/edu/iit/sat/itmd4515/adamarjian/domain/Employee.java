 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author adama
 */
@Entity
public class Employee {    
    
   
    @Id @ GeneratedValue(strategy = GenerationType.IDENTITY)//tells JPA dont expect a value 
    private  Long employeeID;
    private String firstName;
    private String lastName;

    /**
     *
     */
    public Employee() {
    }
    
    /**
     *
     * @param firstName
     * @param lastName
     */
    public Employee( String firstName, String lastName) {

        this.firstName = firstName;
        this.lastName = lastName;
    }
    
    /**
     *
     * @return
     */
    public Long getEmployeeID() {
        return employeeID;
    }

    /**
     *
     * @param employeeID
     */
    public void setEmployeeID(Long employeeID) {
        this.employeeID = employeeID;
    }

    /**
     *
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Employee{" + "employeeID=" + employeeID + ", firstName=" + firstName + ", lastName=" + lastName + '}';
    }
      
}
